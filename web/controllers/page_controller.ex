defmodule Rumbl.PageController do
  use Rumbl.Web, :controller

  def index(conn, _params) do
    require IEx
    IEx.pry
    render conn, "index.html"
  end
end
